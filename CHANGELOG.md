
`should` adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## 2.0.0 (2019-04-01)
- (API change) Commands and tests can be interleaved, each test flushing the command outputs #36
- (API change) Passing 'f' tests now trigger a fail (TODO-but-ok) #20
- (new) 'a' tests, tests "allowed to fail", unless --fail-a #27
- (new) 'i' tests, ignoring case change
- (new) options to select some tests --no-f, --no-a, --only-f, --only-a #28
- (new) --timeout option #4
- More portable colors, works both on light and dark background #35
- Bugs closed: timeout on large outputs #19, --var and --mod overriding from command line #21 #43
- Code refactors #14 #36
- Updated and improved documentation

## 1.0.0 (2018-07-06)
- Initial release, refactor of an earlier shell script


